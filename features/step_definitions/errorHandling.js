/* jslint node: true */
'use strict';

module.exports = function () {

    this.Then(/^I should get a (\d{3}) error with message "(.*)" and code "(.*)"$/, function (statusCode, errorMessage, errorCode, callback) {
        var assertion = this.apickli.assertPathInResponseBodyMatchesExpression('$.message', errorMessage);
        if (!assertion.success) {
            callback(JSON.stringify(assertion));
            return;
        }

        assertion = this.apickli.assertPathInResponseBodyMatchesExpression('$.code', errorCode);
        if (!assertion.success) {
            callback(JSON.stringify(assertion));
            return;
        }

        assertion = this.apickli.assertResponseCode(statusCode);
        if (assertion.success) {
            callback();
        } else {
            callback(JSON.stringify(assertion));
        }

    });

    this.Then(/^I should get at least 1 (\d{3}) error with message "(.*)" and code "(.*)"$/, function (statusCode, errorMessage, errorCode, callback) {
        var numberOfResponsesThatMatch = 0;
        var responses = this.apickli.scenarioVariables.allResponses;

        for (var i = 0; i < responses.length; i++) {
            var r = responses[i];
            var bodyJson = JSON.parse(r.body);

            if ((r.statusCode === parseInt(statusCode)) &&
				(bodyJson.message === errorMessage) &&
				(bodyJson.code === errorCode)) {
                numberOfResponsesThatMatch++;
            }
        }

        if (numberOfResponsesThatMatch > 0) {
            callback();
        } else {
            callback('got no responses that match the criteria...');
        }
    });

};


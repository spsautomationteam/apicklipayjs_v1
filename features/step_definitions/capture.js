/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

var createAuthorization = function (apickli, callback) {
    var pathSuffix = "/charges?type=Authorization";
    var url = apickli.domain + pathSuffix;
    var body = { ECommerce: { Amounts: { Total: 1.0 }, CardData: { Expiration: 1216, Number: 4111111111111111 } } }
    var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, function (err, response) {
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'authorizationReference');
        apickli.setRequestBody("");
        callback();
    });
};

var captureAuthorization = function (apickli, authorizationReference, amount, callback) {
    var pathSuffix = "/charges/" + authorizationReference;
    var url = apickli.domain + pathSuffix;
    var body = { amounts: { total: amount, tax: 1, shipping: 2 } }
    var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "PUT", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.put(pathSuffix, callback);
};

var getCharge = function (apickli, authorizationReference, callback) {
    var pathSuffix = "/charges/" + authorizationReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.get(pathSuffix, callback);
};

module.exports = function () {
    this.Given(/^I have valid merchant credentials$/, function (callback) {
        this.apickli.storeValueInScenarioScope("merchantId", this.apickli.scenarioVariables.merchantId);
        this.apickli.storeValueInScenarioScope("merchantKey", this.apickli.scenarioVariables.merchantKey);
        callback();
    });

    this.Given(/^I create an authorization charge$/, function (callback) {
        createAuthorization(this.apickli, callback);
    });

    this.When(/^I capture that charge with a (.*) dollar amount$/, function (amount, callback) {
        var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
        // console.log("REFERENCE: ", authorizationReference );
        captureAuthorization(this.apickli, authorizationReference, amount, callback);
    });

    this.When(/^I get that same charge$/, function (callback) {
        var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
        // console.log("REFERENCE: ", authorizationReference );
        getCharge(this.apickli, authorizationReference, callback);
    });
};

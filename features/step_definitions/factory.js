var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.paymentsjs.basepath;
var defaultDomain = config.paymentsjs.domain;

console.log('bankcard api: [' + config.paymentsjs.domain + ', ' + config.paymentsjs.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;

	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;

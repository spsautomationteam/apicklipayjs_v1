@intg
Feature: Card Payments
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can run credit card payments
    
    @SimpleSale
    Scenario Outline: basic sale transaction
        Given I have valid credentials
        And I have a valid request
        And I generate an AuthKey
        And I set cardNumber to <CardNumber> 
        And I set cardExpirationDate to 1220
        And I set cvv to 123
		#And I set amount to 9    -- to get 401 rror
        When I POST to /api/payment/card
        Then response code should be 201
        And response body should be valid json
        And response body should contain APPROVED
	Examples:
		|	CardName	|	CardNumber		 |
		|	Visa		|	4111111111111111 |
		|	Mastercard	|	5454545454545454 |
		|	Discover	|	6011000993026909 |
		|	Amex		|	371449635392376  |
	
	
	# ++++++++++++++ Negative Scenarios +++++++++++++++++++++++	
		
	@InvalidExpiryDate
    Scenario: Try to perform a sale transaction with invalid Expiry date
        Given I have valid credentials
        And I have a valid request
        And I generate an AuthKey
        And I set cardNumber to 4111111111111111
        And I set cardExpirationDate to 1215
        And I set cvv to 123
        When I POST to /api/payment/card
        Then response code should be 404
        And response body should be valid json
        And response body should contain Declined
		
	@cardPaymentSale_byExceedLimitCVV
    Scenario Outline: basic sale transaction with Exceed limit of the CVV range
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": <CardNumber>, "expiration": "1220", "cvv":<CVV> }
        When I POST to /api/payment/card
        Then response code should be 404
        And response body should be valid json
        And response body should contain There was a problem with the request. Please see 'detail' for more.
		Examples:
		|	CardName	|	CardNumber		 | CVV   |
		|	Visa		|	4111111111111111 | 2222  |
		|	Mastercard	|	5454545454545454 | 3333  |
		|	Discover	|	6011000993026909 | 4444  |
		|	Amex		|	371449635392376  | 555   |
		
		
	@InvalidCVV
    Scenario: Try to perform a sale transaction with invalid cvv 
        Given I have valid credentials
        And I have a valid request
        And I generate an AuthKey
        And I set cardNumber to 4111111111111111
        And I set cardExpirationDate to 1220
        And I set cvv to 1234567
        When I POST to /api/payment/card
        Then response code should be 404
        And response body should be valid json
        And response body should contain There was a problem with the request. Please see 'detail' for more.
		
	@InvalidCardNumber
    Scenario: Try to perform a sale transaction with invalid cardnumber 
        Given I have valid credentials
        And I have a valid request
        And I generate an AuthKey
        And I set cardNumber to 9111111111111119
        And I set cardExpirationDate to 1220
        And I set cvv to 123
        When I POST to /api/payment/card
        Then response code should be 404
        And response body should be valid json
        And response body should contain There was a problem with the request. Please see 'detail' for more.
		
	

		
		